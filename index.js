/*
    Create functions which can manipulate our arrays.
*/

let registeredUsers = [

    "James Jeffries",
    "Gunther Smith",
    "Macie West",
    "Michelle Queen",
    "Shane Miguelito",
    "Fernando Dela Cruz",
    "Akiko Yukihime"
];

let friendsList = [];


function register(username) {
   let alreadyUser = registeredUsers.includes(username);

   if (alreadyUser) {
        alert("Registration Failed. username Already Exists!")
   }
   else {
        registeredUsers.push(username);
        alert("Thank you for registering!");
   }
}


function add(username) {
    let foundUser = registeredUsers.includes(username);

    if (foundUser){
        friendsList.push(username);
        alert("You have added " + username + " as a friend!")
    }
    else{
        alert("User not found!")
    }
}

function displayFriends(){
    if(friendsList.length == 0){
        alert("You have 0 friends. Add one first.")
    }
   
    else{
        friendsList.forEach(function(friend){
                console.log(friend)});
        }
}

function numberFriends(){
    if(friendsList.length == 0){
        alert("You have 0 friends. Add one first.")
    }

    else{
        alert("You currently have " + friendsList.length + " friends")
    }
}


function deleteUsers(){
    if(friendsList.length > 0){
        friendsList.pop();
    }
    else{
        alert("You currently have 0 friends. Add one first.")
    }
}